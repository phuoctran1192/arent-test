import { useQuery } from "@tanstack/react-query"
import { BaseService } from "../api/base.service"
import { API_QUERY } from "../query"

export const exerciseQuery = () => {
  return {
    queryKey: [API_QUERY.GET_EAT],
    queryFn: async () => BaseService.get("/todos"),
    select: (data) => {
      return data.map((item) => ({ ...item, time : '10', stat: '26kcal'  }))
    },
    retry: false,
  }
}

export const useExercise = (props) => useQuery(exerciseQuery(props))
