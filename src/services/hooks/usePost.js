import { useQuery } from "@tanstack/react-query"
import { BaseService } from "../api/base.service"
import { API_QUERY } from "../query"

export const postQuery = () => {
 
  return {
    queryKey: [API_QUERY.GET_EAT],
    queryFn: async () => BaseService.get("/photos"),
    select: (data) => {
      return data.map((item) => ({ ...item, tag: '#魚料理  #和食  #DHA' }))
    },
    retry: false,
  }
}

export const usePost = (props) => useQuery(postQuery(props))
