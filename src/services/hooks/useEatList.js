import { useQuery } from "@tanstack/react-query"
import { BaseService } from "../api/base.service"
import { API_QUERY } from "../query"

export const eatListQuery = () => {
  const getRandomLabel = () => {
    const values = ["morning", "lunch", "dinner", "snack"]
    const randomIndex = Math.floor(Math.random() * values.length)
    return values[randomIndex]
  }
  return {
    queryKey: [API_QUERY.GET_EAT],
    queryFn: async () => BaseService.get("/photos"),
    select: (data) => {
      return data.map((item) => ({ ...item, label: getRandomLabel() }))
    },
    retry: false,
  }
}

export const useEatList = (props) => useQuery(eatListQuery(props))
