import { useMemo, useState } from "react";
import HexagonImage from "../../assets/images/hexagon.jpg";
import KnifeIcon from "../../assets/icon/icon_knife.svg";
import SnackIcon from "../../assets/icon/icon_cup.svg";
import ButtonBackground from "../../assets/icon/button.svg";
import { Button, Radio, Spin } from "antd";
import { useEatList } from "../../services/hooks/useEatList";

export default function EatList() {
  const { data = [], isLoading } = useEatList();
  const [filter, setFilter] = useState();
  const [page, setPage] = useState(1);

  const paginationData = useMemo(() => {
    return data.slice(0, page * 8) ?? [];
  }, [data, page]);

  const dataFilter = useMemo(() => {
    if (!filter) return paginationData;
    return paginationData.filter((item) => item.label === filter) ?? [];
  }, [paginationData, filter]);

  const eatListStyle = {
    filterContainer: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      alignItems: "center",
      marginTop: "21px",
      marginBottom: "24px",
    },
    filterItem: {
      width: "136px",
      height: "136px",
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      textAlign: "center",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      margin: "8 32px",
      cursor: "pointer",
    },
    filterItemText: {
      color: "#FFF",
      textAlign: "center",
      fontFamily: "Inter",
      fontSize: "20px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "24px",
    },
    eatList: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      alignItems: "center",
    },
    eatListItem: {
      display: "block",
      width: "234px",
      height: "234px",
      overflow: "hidden",
      position: "relative",
      background: "#000",
      marginBottom: "8px",
    },
    eatListItemImage: {
      maxWidth: "100%",
    },
    eatListItemTitle: {
      color: "#FFF",
      fontFamily: "Inter",
      fontSize: "15px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.15px",
      position: "absolute",
      bottom: 0,
      left: 0,
      background: "#FFCC21",
      padding: "8px",
    },
    eatListLoadMore: {
      width: "296px",
      height: "56px",
      backgroundImage: "url(" + ButtonBackground + ")",
      border: "none",
      color: "#FFF",
      textAlign: "center",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "26px",
      display: "block",
      margin: "42px auto",
    },
  };

  return (
    <div className="container">
      <Radio.Group
        className="radio-custom"
        onChange={(e) => setFilter(e.target.value)}
        style={eatListStyle.filterContainer}
      >
        <Radio value={"morning"}>
          <div
            className="filterItem"
            style={{
              backgroundImage: "url(" + HexagonImage + ")",
              ...eatListStyle.filterItem,
            }}
          >
            <div style={eatListStyle.filterItemText}>
              <img src={KnifeIcon} alt="Morning" />
              <br />
              Morning
            </div>
          </div>
        </Radio>
        <Radio value={"lunch"}>
          <div
            className="filterItem"
            style={{
              backgroundImage: "url(" + HexagonImage + ")",
              ...eatListStyle.filterItem,
            }}
          >
            <div style={eatListStyle.filterItemText}>
              <img src={KnifeIcon} alt="Lunch" />
              <br />
              Lunch
            </div>
          </div>
        </Radio>
        <Radio value={"dinner"}>
          <div
            className="filterItem"
            style={{
              backgroundImage: "url(" + HexagonImage + ")",
              ...eatListStyle.filterItem,
            }}
          >
            <div style={eatListStyle.filterItemText}>
              <img src={KnifeIcon} alt="Dinner" />
              <br />
              Dinner
            </div>
          </div>
        </Radio>
        <Radio value={"snack"}>
          <div
            className="filterItem"
            style={{
              backgroundImage: "url(" + HexagonImage + ")",
              ...eatListStyle.filterItem,
            }}
          >
            <div style={eatListStyle.filterItemText}>
              <img src={SnackIcon} alt="Snack" />
              <br />
              Snack
            </div>
          </div>
        </Radio>
      </Radio.Group>

      <div className="eatList" style={eatListStyle.eatList}>
        {isLoading && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
              width: "100%",
            }}
          >
            <Spin size="large" />
          </div>
        )}
        {dataFilter.map((item) => (
          <div
            className="eatListItem"
            style={eatListStyle.eatListItem}
            key={item.id}
          >
            <div className="eatListItemImage">
              <img
                style={eatListStyle.eatListItemImage}
                src={item.url}
                alt={item.title}
              />
            </div>
            <div
              className="eatListItemTitle"
              style={eatListStyle.eatListItemTitle}
            >
              {item.label}
            </div>
          </div>
        ))}
        <span style={{ width: "234px" }}></span>
        <span style={{ width: "234px" }}></span>
        <span style={{ width: "234px" }}></span>
        <span style={{ width: "234px" }}></span>
      </div>
      <Button
        style={eatListStyle.eatListLoadMore}
        onClick={() => setPage(page + 1)}
      >
        記録をもっと見る
      </Button>
    </div>
  );
}
