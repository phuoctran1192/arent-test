import { useQuery } from "@tanstack/react-query"
import { BaseService } from "../api/base.service"
import { API_QUERY } from "../query"

export const diaryQuery = () => {
  const date = new Date();
  return {
    queryKey: [API_QUERY.GET_EAT],
    queryFn: async () => BaseService.get("/posts"),
    select: (data) => {
      return data.map((item) => ({ ...item, date : date.toISOString()  }))
    },
    retry: false,
  }
}

export const useDiary = (props) => useQuery(diaryQuery(props))
