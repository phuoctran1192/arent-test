import { useMemo, useState } from "react";
import ButtonBackground from "../../assets/icon/button.svg";
import RecommendOne from "../../assets/images/MyRecommend-1.jpg";
import RecommendTwo from "../../assets/images/MyRecommend-2.jpg";
import RecommendThree from "../../assets/images/MyRecommend-3.jpg";
import { Button, Spin } from "antd";
import { ReactECharts } from "../../component/Echart";
import { useDiary } from "../../services/hooks/useDiary";
import { useExercise } from "../../services/hooks/useExercise";

export default function MyRecord() {
  const { data = [], isLoading } = useDiary();
  const { data: exerciseData = [], isLoading: isExerciseLoading } =
    useExercise();
  const [page, setPage] = useState(1);

  const paginationData = useMemo(() => {
    return data.slice(0, page * 8) ?? [];
  }, [data, page]);

  const option = {
    tooltip: {
      trigger: "axis",
    },
    grid: {
      show: false,
      top: "4%",
      left: "0%",
      right: "3%",
      bottom: "4%",
      containLabel: true,
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: [
        "6 月",
        "7 月",
        "8 月",
        "9 月",
        "10 月",
        "11 月",
        "12 月",
        "1 月",
        "2 月",
        "3 月",
        "4 月",
        "5 月",
      ],
      axisLabel: {
        textStyle: {
          color: "#fff",
        },
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        textStyle: {
          color: "transparent",
        },
      },
    },
    series: [
      {
        name: "Email",
        type: "line",
        stack: "Total",
        data: [520, 132, 101, 134, 90, 230, 210, 134, 90, 230, 210, 134],
        lineStyle: { color: "#FFCC21", width: 4 },
        itemStyle: {
          color: "#FFCC21",
        },
        symbolSize: 10,
      },
      {
        name: "Union Ads",
        type: "line",
        stack: "Total",
        data: [520, 182, 191, 234, 290, 330, 310, 134, 90, 230, 210, 134],
        lineStyle: { color: "#8FE9D0", width: 4 },
        itemStyle: {
          color: "#8FE9D0",
        },
        symbolSize: 10,
      },
    ],
  };

  const style = {
    heading: {
      color: "#414141",
      fontFamily: "Inter",
      fontSize: "22px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "27px",
      letterSpacing: "0.11px",
      margin: 0,
    },
    record: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      margin: "56px auto",
    },
    recordItemImgWrapper: {
      position: "relative",
    },
    recordItemImgOverlay: {
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      opacity: 0.4,
      background: "#000",
      mixBlendMode: "luminosity",
    },
    recordItemImg: {
      display: "block",
      width: "240px",
      height: "240px",
    },
    recordContent: {
      position: "absolute",
      zIndex: 2,
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
    },
    recordTitle: {
      color: "#FFCC21",
      textAlign: "center",
      fontFamily: "Inter",
      fontSize: "25px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "30px",
      letterSpacing: "0.125px",
      marginBottom: "8px",
      whiteSpace: "nowrap",
    },
    recordSubtitle: {
      color: "#FFF",
      textAlign: "center",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "20px",
      background: "#FF963C",
    },
    recordItem: {
      cursor: "pointer",
      backgroundColor: "#FFCC21",
      padding: "24px",
      color: "white",
      textAlign: "center",
      position: "relative",
      marginBottom: "8px",
    },
    diary: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      alignItems: "center",
    },
    diaryItem: {
      display: "block",
      width: "231px",
      height: "231px",
      border: "2px solid #707070",
      padding: "27px 16px",
      marginBottom: "12px",
    },
    dateTime: {
      color: "#414141",
      fontFamily: "Inter",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "22px",
      letterSpacing: "0.09px",
    },
    diaryTitle: {
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      WebkitLineClamp: "2",
      color: "#414141",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "12px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "17px",
      letterSpacing: "0.06px",
      marginTop: "8px",
    },
    diaryContent: {
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      WebkitLineClamp: "5",
      WebkitBoxOrient: "vertical",
      color: "#414141",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "12px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "17px",
      letterSpacing: "0.06px",
    },
    loadMore: {
      width: "296px",
      height: "56px",
      backgroundImage: "url(" + ButtonBackground + ")",
      border: "none",
      color: "#FFF",
      textAlign: "center",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "26px",
      display: "block",
      margin: "42px auto",
    },
    sectionTitle: {
      color: "#FFF",
      display: "flex",
      justifyContent: "start",
      alignItems: "center",
      marginBottom: "4px",
    },
    sectionTime: {
      color: "#FFF",
      fontFamily: "Inter",
      fontSize: "22px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "27px",
      letterSpacing: "0.11px",
    },
    sectionHeading: {
      color: "#FFF",
      fontFamily: "Inter",
      fontSize: "15px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.15px",
      margin: "0",
      width: "96px",
    },
    chart: {
      padding: "16px 24px",
      background: "#414141",
      color: "#FFF",
      margin: "56px 0",
    },
    chartLegend: {
        listStyle: "none",
      display: "flex",
      justifyContent: "start",
      alignItems: "center",
      padding: "0",
    },
    chartLegendItem: {
        width: "56px",
        height: "24px",
        borderRadius: "11px",
      background: "#FFF", 
      color: "#FFCC21",
      textAlign: "center",  
      marginRight: "16px",
    },
    exercise: {
      padding: "16px 24px",
      background: "#414141",
      color: "#FFF",
      margin: "56px 0",
    },
    exerciseList: {
      height: "264px",
      overflowY: "scroll",
      display: "flex",
      justifyContent: "space-between",
      flexWrap: "wrap",
      padding: "0",
      listStyle: "none",
    },
    exerciseItem: {
      flex: "0 0 48%",
      borderBottom: "1px solid #777",
      paddingLeft: '16px',
      position: "relative",
      marginBottom: "8px",
    },
    exerciseItemDot: {
      display: "inline-block",
      width: "4px",
      height: "4px",
      borderRadius: "50%",
      position: "absolute",
      top: "11px",
      left: "0",
      backgroundColor: "#FFF"
    },
    exerciseItemWrap: {
      display: "flex",
      justifyContent: "space-between",
    },
    exerciseTitle: {
      color: "#FFF",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      WebkitLineClamp: "1",
      WebkitBoxOrient: "vertical",
    },
    exerciseStat: {
      color: "#FFCC21",
      fontFamily: "Inter",
      fontSize: "15px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.075px",
    },
    exerciseTime: {
      color: "#FFCC21",
      textAlign: "right",
      fontFamily: "Inter",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "22px",
      letterSpacing: "0.09px",
      whiteSpace: "nowrap",
    },
  };

  return (
    <div className="container" style={{}}>
      <div className="record" style={style.record}>
        <a href="#record">
          <div className="recordItem" style={style.recordItem}>
            <div
              className="recordItemImgWrapper"
              style={style.recordItemImgWrapper}
            >
              <img
                style={style.recordItemImg}
                src={RecommendOne}
                alt="BODY RECORD"
              />
              <div
                className="recordItemImgOverlay"
                style={style.recordItemImgOverlay}
              ></div>
            </div>
            <div className="recordContent" style={style.recordContent}>
              <div className="recordTitle" style={style.recordTitle}>
                BODY RECORD
              </div>
              <div style={style.recordBreak}></div>
              <div className="recordSubtitle" style={style.recordSubtitle}>
                自分のカラダの記録
              </div>
            </div>
          </div>
        </a>
        <a href="#exercise">
          <div className="recordItem" style={style.recordItem}>
            <div
              className="recordItemImgWrapper"
              style={style.recordItemImgWrapper}
            >
              <img
                style={style.recordItemImg}
                src={RecommendTwo}
                alt="MY EXERCISE"
              />
              <div
                className="recordItemImgOverlay"
                style={style.recordItemImgOverlay}
              ></div>
            </div>
            <div className="recordContent" style={style.recordContent}>
              <div className="recordTitle" style={style.recordTitle}>
                MY EXERCISE
              </div>
              <div style={style.recordBreak}></div>
              <div className="recordSubtitle" style={style.recordSubtitle}>
                自分の運動の記録
              </div>
            </div>
          </div>
        </a>
        <a href="#diary">
          <div className="recordItem" style={style.recordItem}>
            <div
              className="recordItemImgWrapper"
              style={style.recordItemImgWrapper}
            >
              <img
                style={style.recordItemImg}
                src={RecommendThree}
                alt="MY DIARY"
              />
              <div
                className="recordItemImgOverlay"
                style={style.recordItemImgOverlay}
              ></div>
            </div>
            <div className="recordContent" style={style.recordContent}>
              <div className="recordTitle" style={style.recordTitle}>
                MY DIARY
              </div>
              <div style={style.recordBreak}></div>
              <div className="recordSubtitle" style={style.recordSubtitle}>
                自分の日記
              </div>
            </div>
          </div>
        </a>
      </div>
      <div id="record" className="chart" style={style.chart}>
      <div className="sectionTitle" style={style.sectionTitle}>
          <h2 style={style.sectionHeading}>BODY RECORD</h2>
          <div style={style.sectionTime}>2021.05.21</div>
        </div>
        <ReactECharts
          option={option}
          style={{ with: "100%", height: "304px" }}
        />
        <ul style={style.chartLegend}>
            <li style={style.chartLegendItem}>日</li>
            <li style={style.chartLegendItem}>週</li>
            <li style={style.chartLegendItem}>月</li>
            <li style={style.chartLegendItem}>年</li>
        </ul>
      </div>
      <div id="exercise" className="exercise" style={style.exercise}>
        <div className="sectionTitle" style={style.sectionTitle}>
          <h2 style={style.sectionHeading}>MY EXERCISE</h2>
          <div style={style.sectionTime}>2021.05.21</div>
        </div>
        {isExerciseLoading && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
              width: "100%",
            }}
          >
            <Spin size="large" />
          </div>
        )}
        <ul style={style.exerciseList}>
          {exerciseData.map((item) => (
            <li style={style.exerciseItem} key={item.id}>
              <div style={style.exerciseItemDot}></div>
              <div style={style.exerciseItemWrap}>
                <div>
                  <div style={style.exerciseTitle}>{item.title}</div>
                  <div style={style.exerciseStat}>{item.stat}</div>
                </div>
                <div style={style.exerciseTime}>{item.time} min</div>
              </div>
            </li>
          ))}
        </ul>
      </div>

      <h2 id="diary" style={style.heading}>
        MY DIARY
      </h2>
      <div className="diary" style={style.diary}>
        {isLoading && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
              width: "100%",
            }}
          >
            <Spin size="large" />
          </div>
        )}
        {paginationData.map((item) => (
          <div className="diary" style={style.diary} key={item.id}>
            <div className="diaryItem" style={style.diaryItem}>
              <div style={style.dateTime}>
                {new Date(item.date).toISOString().split("T")[0]}
              </div>
              <div style={style.dateTime}>
                {new Date(item.date)
                  .toLocaleString()
                  .split(" ")[1]
                  .split(":")
                  .splice(0, 2)
                  .join(":")}
              </div>
              <div style={style.diaryTitle}>{item.title}</div>
              <div style={style.diaryContent}>{item.body}</div>
            </div>
          </div>
        ))}
        <span style={{ width: "234px" }}></span>
      </div>
      <Button style={style.loadMore} onClick={() => setPage(page + 1)}>
        自分の日記をもっと見る
      </Button>
    </div>
  );
}
