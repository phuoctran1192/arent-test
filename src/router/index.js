import * as React from "react"
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import ErrorPage from "../page/error"
import Root from "./layout"
import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import MyPage from "../page/MyPage"
import Column from "../page/Column"
import MyRecord from "../page/MyRecord"

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 30, // 30 seconds
      refetchOnMount: true,
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
    },
  },
})

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <MyPage />,
      },
      {
        path: "myRecord/",
        element: <MyRecord />,
      },
      {
        path: "column/",
        element: <Column />,
      },
    ],
  },
])
const Router = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  )
}
export default Router
