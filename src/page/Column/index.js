import { useMemo, useState } from "react";
import ButtonBackground from "../../assets/icon/button.svg";
import { Button, Spin } from "antd";
import { usePost } from "../../services/hooks/usePost";

export default function Column() {
  const { data = [], isLoading } = usePost();
  const [page, setPage] = useState(1);

  const paginationData = useMemo(() => {
    return data.slice(0, page * 8) ?? [];
  }, [data, page]);

  const columnStyle = {
    recommended: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      margin: "56px auto",
    },
    recommendedItem: {
      backgroundColor: "#2E2E2E",
      padding: "24px 8px",
      color: "white",
      textAlign: "center",
      width: "216px",
      height: "144px",
      marginBottom: "8px",
    },
    recommendedItemTitle: {
      color: "#FFCC21",
      textAlign: "center",
      fontFamily: "Inter",
      fontSize: "22px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "27px",
      letterSpacing: "0.11px",
    },
    recommendedItemBreak: {
      width: "56px",
      height: "1px",
      backgroundColor: "#FFF",
      display: "block",
      margin: "10px auto 8px auto",
    },
    column: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      alignItems: "center",
    },
    columnItem: {
      marginBottom: "8px",
      display: "block",
      width: "234px",
    },
    columnItemImageContainer: {
      display: "block",
      width: "234px",
      height: "145px",
      overflow: "hidden",
      position: "relative",
    },
    columnItemImage: {
      maxWidth: "100%",
    },
    columnItemTag: {
      color: "#FF963C",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "12px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "22px",
      letterSpacing: "0.06px",
    },
    columnItemTitle: {
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      WebkitLineClamp: "2",
      WebkitBoxOrient: "vertical",
      color: "#414141",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "15px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "22px",
      letterSpacing: "0.075px",
      height: "48px",

      marginTop: "8px",
    },
    columnItemLabel: {
      color: "#FFF",
      fontFamily: "Inter",
      fontSize: "15px",
      fontStyle: "normal",
      fontWeight: 400,
      lineHeight: "18px",
      letterSpacing: "0.15px",
      position: "absolute",
      bottom: 0,
      left: 0,
      background: "#FFCC21",
      padding: "8px",
    },
    columnLoadMore: {
      width: "296px",
      height: "56px",
      backgroundImage: "url(" + ButtonBackground + ")",
      border: "none",
      color: "#FFF",
      textAlign: "center",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "26px",
      display: "block",
      margin: "42px auto",
    },
  };

  return (
    <div className="container" style={{}}>
      <div className="recommended" style={columnStyle.recommended}>
        <div className="recommendedItem" style={columnStyle.recommendedItem}>
          <div
            className="recommendedItemTitle"
            style={columnStyle.recommendedItemTitle}
          >
            RECOMMENDED COLUMN
          </div>
          <div style={columnStyle.recommendedItemBreak}></div>
          <div
            className="recommendedItemSubtitle"
            style={columnStyle.recommendedItemSubtitle}
          >
            オススメ
          </div>
        </div>
        <div className="recommendedItem" style={columnStyle.recommendedItem}>
          <div
            className="recommendedItemTitle"
            style={columnStyle.recommendedItemTitle}
          >
            RECOMMENDED DIET
          </div>
          <div style={columnStyle.recommendedItemBreak}></div>
          <div
            className="recommendedItemSubtitle"
            style={columnStyle.recommendedItemSubtitle}
          >
            ダイエット
          </div>
        </div>
        <div className="recommendedItem" style={columnStyle.recommendedItem}>
          <div
            className="recommendedItemTitle"
            style={columnStyle.recommendedItemTitle}
          >
            RECOMMENDED BEAUTY
          </div>
          <div style={columnStyle.recommendedItemBreak}></div>
          <div
            className="recommendedItemSubtitle"
            style={columnStyle.recommendedItemSubtitle}
          >
            美容
          </div>
        </div>
        <div className="recommendedItem" style={columnStyle.recommendedItem}>
          <div
            className="recommendedItemTitle"
            style={columnStyle.recommendedItemTitle}
          >
            RECOMMENDED HEALTH
          </div>
          <div style={columnStyle.recommendedItemBreak}></div>
          <div
            className="recommendedItemSubtitle"
            style={columnStyle.recommendedItemSubtitle}
          >
            健康
          </div>
        </div>
      </div>
      <div className="column" style={columnStyle.column}>
        {isLoading && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "50vh",
              width: "100%",
            }}
          >
            <Spin size="large" />
          </div>
        )}
        {paginationData.map((item) => (
          <div
            className="columnItem"
            style={columnStyle.columnItem}
            key={item.id}
          >
            <div
              className="columnItemImageContainer"
              style={columnStyle.columnItemImageContainer}
            >
              <div className="columnItemImage">
                <img
                  style={columnStyle.columnItemImage}
                  src={item.url}
                  alt={item.title}
                />
              </div>
              <div
                className="columnItemLabel"
                style={columnStyle.columnItemLabel}
              >
                {item.label}
              </div>
            </div>
            <div style={columnStyle.columnItemTitle}>{item.title}</div>
            <div style={columnStyle.columnItemTag}>{item.tag}</div>
          </div>
        ))}
        <span style={{ width: "234px" }}></span>
      </div>
      <Button
        style={columnStyle.columnLoadMore}
        onClick={() => setPage(page + 1)}
      >
        コラムをもっと見る
      </Button>
    </div>
  );
}
