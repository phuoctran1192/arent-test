import HeroImage from "../../assets/images/main_photo.jpg";
import { ReactECharts } from "../../component/Echart";
import EatList from "./EatList";

export default function MyPage() {
  const option = {
    tooltip: {
      trigger: "axis",
    },
    grid: {
      show: false,
      left: "3%",
      right: "3%",
      bottom: "3%",
      containLabel: true,
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: [
        "6 月",
        "7 月",
        "8 月",
        "9 月",
        "10 月",
        "11 月",
        "12 月",
        "1 月",
        "2 月",
        "3 月",
        "4 月",
        "5 月",
      ],
      axisLabel: {
        textStyle: {
          color: "#fff",
        },
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        textStyle: {
          color: "transparent",
        },
      },
    },
    series: [
      {
        name: "Email",
        type: "line",
        stack: "Total",
        data: [520, 132, 101, 134, 90, 230, 210, 134, 90, 230, 210, 134],
        lineStyle: { color: "#FFCC21", width: 4 },
        itemStyle: {
          color: "#FFCC21",
        },
        symbolSize: 10,
      },
      {
        name: "Union Ads",
        type: "line",
        stack: "Total",
        data: [520, 182, 191, 234, 290, 330, 310, 134, 90, 230, 210, 134],
        lineStyle: { color: "#8FE9D0", width: 4 },
        itemStyle: {
          color: "#8FE9D0",
        },
        symbolSize: 10,
      },
    ],
  };
  const gaugeData = [
    {
      value: 60,
      title: {
        offsetCenter: ["0%", "30%"],
      },
      detail: {
        valueAnimation: true,
        offsetCenter: ["0%", "5%"],
      },
    },
  ];
  const gaugeoption = {
    series: [
      {
        type: "gauge",
        startAngle: 90,
        endAngle: -270,
        pointer: {
          show: false,
        },
        progress: {
          show: true,
          overlap: false,
          roundCap: true,
          clip: false,
          itemStyle: {
            borderWidth: 1,
            borderColor: "#FFF",
          },
        },
        axisLine: {
          lineStyle: {
            width: 4,
          },
        },
        splitLine: {
          show: false,
          distance: 0,
          length: 10,
        },
        axisTick: {
          show: false,
        },
        axisLabel: {
          show: false,
          distance: 50,
        },
        data: gaugeData,
        title: {
          fontSize: 14,
        },
        detail: {
          width: 50,
          height: 14,
          fontSize: 14,
          color: "#FFF",
          borderColor: "#FFF",
          borderRadius: 20,
          borderWidth: 1,
          formatter: "{value}%",
        },
      },
    ],
  };
  return (
    <div className="container1280">
      <div
        className="hero"
        style={{ display: "flex", maxWidth: "100%", overflow: "hidden" }}
      >
        <div className="rightChart" style={{ position: "relative" }}>
          <ReactECharts
            option={gaugeoption}
            style={{
              width: "181px",
              height: "181px",
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              zIndex: 2,
            }}
          />

          <img
            src={HeroImage}
            alt="main"
            style={{ width: "540px", height: "312px" }}
          />
        </div>
        <ReactECharts
          option={option}
          style={{ height: "312px", background: "#2E2E2E" }}
        />
      </div>
      <EatList />
    </div>
  );
}
