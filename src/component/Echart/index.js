import { getInstanceByDom, init } from "echarts"
import { useEffect, useRef } from "react"

import useSize from "@react-hook/size"
import { BarChart, LineChart, PieChart, ScatterChart } from "echarts/charts"
import {
  DataZoomComponent,
  GridComponent,
  LegendComponent,
  TitleComponent,
  ToolboxComponent,
  TooltipComponent,
} from "echarts/components"
import { use } from "echarts/core"
import { CanvasRenderer } from "echarts/renderers"

use([
  LegendComponent,
  ScatterChart,
  LineChart,
  BarChart,
  PieChart,
  GridComponent,
  TooltipComponent,
  TitleComponent,
  ToolboxComponent,
  DataZoomComponent,
  CanvasRenderer,
])

export function ReactECharts({ option, style, settings, loading, theme }) {
  const chartRef = useRef(null)
  const [width, height] = useSize(chartRef)

  useEffect(() => {
    if (chartRef.current !== null) {
      const chart = init(chartRef.current, theme)
      chart?.resize()
    }
    const chart = getInstanceByDom(chartRef.current)
    return () => {
      if (chart) {
        chart?.dispose()
      }
    }
  }, [theme])

  useEffect(() => {
    if (chartRef.current !== null) {
      const chart = getInstanceByDom(chartRef.current)
      chart?.resize({
        opts: { animation: { easing: "cubic-bezier(0.3, 0.5, 0, 1) 0s" } },
      })
    }
  }, [width, height])

  useEffect(() => {
    // Update chart
    if (chartRef.current !== null) {
      const chart = getInstanceByDom(chartRef.current)
      chart?.setOption(option, settings ?? true)
    }
  }, [option, settings, theme])

  useEffect(() => {
    // Update chart
    if (chartRef.current !== null) {
      const chart = getInstanceByDom(chartRef.current)
      loading === true ? chart?.showLoading() : chart?.hideLoading()
    }
  }, [loading, theme])

  return (
    <div ref={chartRef} style={{ width: "100%", height: "100%", ...style }} />
  )
}
