import { Link, Outlet } from "react-router-dom";
import Logo from "../assets/icon/logo.svg";
import MeMo from "../assets/icon/icon_memo.svg";
import Challenge from "../assets/icon/icon_challenge.svg";
import Info from "../assets/icon/icon_info.svg";
import Menu from "../assets/icon/icon_menu.svg";
import Close from "../assets/icon/icon_close.svg";
import { useCallback, useEffect, useRef, useState } from "react";

const DrawMenu = () => {
  const catMenu = useRef(null);
  const [open, setOpen] = useState(false);
  const menuStyle = {
    rootDrawMenu: {
      position: "relative",
    },
    menuButtonStyle: {
      background: "transparent",
      boxShadow: "none",
    },
    drawMenu: {
      position: "absolute",
      zIndex: 99,
      top: "100%",
      right: 0,
      background: "#777",
    },
    linkStyle: {
      width: "280px",
      display: "block",
      borderTop: "1px solid rgb(255 255 255 / 15%)",
      borderBottom: "1px solid rgb(46 46 46 / 25%)",
      padding: "23px 32px",
      textDecoration: "none",
      color: "white",
      fontSize: "18px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "26px",
    },
  };
  const closeOpenMenus = useCallback(
    (e) => {
      if (catMenu.current && open && !catMenu.current.contains(e.target)) {
        setOpen(false);
      }
    },
    [open]
  );
  useEffect(() => {
    document.addEventListener("click", closeOpenMenus);
  }, [closeOpenMenus]);
  return (
    <div style={menuStyle.rootDrawMenu} ref={catMenu}>
      <button style={menuStyle.menuButtonStyle} onClick={() => setOpen(!open)}>
        <img src={open ? Close : Menu} alt="menu" />
      </button>
      {open && (
        <div style={menuStyle.drawMenu}>
          <nav style={menuStyle.menuStyle}>
            <Link
              style={menuStyle.linkStyle}
              to={`/myRecord`}
              onClick={() => setOpen(false)}
            >
              自分の記録
            </Link>
            <Link
              style={menuStyle.linkStyle}
              to={`/`}
              onClick={() => setOpen(false)}
            >
              体重グラフ
            </Link>
            <Link
              style={menuStyle.linkStyle}
              to={`/`}
              onClick={() => setOpen(false)}
            >
              目標
            </Link>
            <Link
              style={menuStyle.linkStyle}
              to={`/`}
              onClick={() => setOpen(false)}
            >
              選択中のコース
            </Link>
            <Link
              style={menuStyle.linkStyle}
              to={`/column`}
              onClick={() => setOpen(false)}
            >
              コラム一覧
            </Link>
            <Link
              style={menuStyle.linkStyle}
              to={`/`}
              onClick={() => setOpen(false)}
            >
              設定
            </Link>
          </nav>
        </div>
      )}
    </div>
  );
};
export default function Root() {
  const rootStyle = {
    layoutStyle: {
      minHeight: "100vh",
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
    },
    linkStyle: {
      color: "white",
      display: "flex",
      alignItems: "center",
      width: "160px",
      padding: "8px",
      textDecoration: "none",
      fontFamily: "Hiragino Kaku Gothic Pro",
      fontSize: "16px",
      fontStyle: "normal",
      fontWeight: 300,
      lineHeight: "23px",
    },
    linkFooterStyle: {
      color: "white",
      textDecoration: "none",
      marginRight: "45px",
    },
    menuStyle: {
      display: "flex",
    },
    iconStyle: {
      display: "block",
      marginRight: "8px",
    },
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div style={rootStyle.layoutStyle}>
      <div
        className="header"
        style={{
          background: "#414141",
          boxShadow: "0px 3px 6px 0px rgba(0, 0, 0, 0.16)",
        }}
      >
        <div className="container">
          <div
            className="menu"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              padding: "8px 0",
            }}
          >
            <Link to={`/`}>
              <img src={Logo} alt="Logo" />
            </Link>
            <nav style={rootStyle.menuStyle}>
              <Link style={rootStyle.linkStyle} to={`/myRecord`}>
                <img style={rootStyle.iconStyle} src={MeMo} alt="my page" />
                自分の記録
              </Link>
              <Link style={rootStyle.linkStyle} to={`/`}>
                <img
                  style={rootStyle.iconStyle}
                  src={Challenge}
                  alt="myRecord"
                />
                チャレンジ
              </Link>
              <Link style={rootStyle.linkStyle} to={`/`}>
                <img
                  style={rootStyle.iconStyle}
                  src={Info}
                  alt="myNotification"
                />
                お知らせ
              </Link>
              <DrawMenu />
            </nav>
          </div>
        </div>
      </div>
      <div id="detail" style={{ flex: "1" }}>
        <Outlet />
      </div>
      <div
        className="footer"
        style={{
          background: "#414141",
          boxShadow: "0px 3px 6px 0px rgba(0, 0, 0, 0.16)",
        }}
      >
        <div className="container">
          <div
            className="menu"
            style={{
              minHeight: "128px",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <nav style={rootStyle.menuStyle}>
              <Link style={rootStyle.linkFooterStyle} to={`/`}>
                会員登録
              </Link>
              <Link style={rootStyle.linkFooterStyle} to={`/myRecord`}>
                運営会社
              </Link>
              <Link style={rootStyle.linkFooterStyle} to={`/myNotification`}>
                利用規約
              </Link>
              <Link style={rootStyle.linkFooterStyle} to={`/myNotification`}>
                個人情報の取扱について
              </Link>
              <Link style={rootStyle.linkFooterStyle} to={`/myNotification`}>
                特定商取引法に基づく表記
              </Link>
              <Link style={rootStyle.linkFooterStyle} to={`/myNotification`}>
                お問い合わせ
              </Link>
            </nav>
          </div>
        </div>
        <div
          className="totop"
          style={{
            position: "fixed",
            bottom: "272px",
            right: "96px",
            cursor: "pointer",
          }}
        >
          <div
            onClick={scrollToTop}
            style={{ textAlign: "center", textDecoration: "none" }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width={48}
              height={48}
              viewBox="0 0 48 48"
              fill="none"
            >
              <path
                d="M24 0.5C36.9787 0.5 47.5 11.0213 47.5 24C47.5 36.9787 36.9787 47.5 24 47.5C11.0213 47.5 0.5 36.9787 0.5 24C0.5 11.0213 11.0213 0.5 24 0.5Z"
                fill="white"
                fillOpacity="0.01"
                stroke="#777777"
              />
              <path
                d="M30.5853 28.042L24.0003 21.6579L17.4153 28.042L16.5391 27.1925L24.0003 19.959L31.4615 27.1925L30.5853 28.042Z"
                fill="#777777"
              />
            </svg>
            <br />
            <span id="totop">Go to Top</span>
          </div>
        </div>
      </div>
    </div>
  );
}
