const BASE_URL = "https://jsonplaceholder.typicode.com"
const get = async (url, headers = {}) => {
  const requestOptions = {
    method: "GET",
    headers: new Headers({
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
      ...headers,
    }),
  }

  const response = await fetch(BASE_URL + url, requestOptions)
  return handleResponse(response)
}

// helper functions
const handleResponse = (response) => {
  return response.ok ? response.json() : Promise.reject(response)
}

export const BaseService = {
  get,
}
